﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestLevelChoreography : MonoBehaviour
{
    public GameObject[] spawner;

    public float levelTimer;
    public float[] levelTimeLimit;
    public int levelIndex;
    public int[] wavesSizes;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        levelTimer += Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.O))
        {
            Spawn(1);
        }
        if (levelTimer >= levelTimeLimit[levelIndex])
        {
            Spawn(wavesSizes[levelIndex]);
            levelIndex++;
            if (levelIndex >= levelTimeLimit.Length)
            {
                levelIndex = 0;
                levelTimer = 0;
            }
        }
    }

    public void Spawn(int amountToSpawn)
    {
        foreach (GameObject spawn in spawner)
        {
            for (int i = 0; i < amountToSpawn; i++)
            {
                CombatManager.instance.CreateNewUnit(CombatManager.UnitType.BaseEnemy, spawn.transform.position);
            }
        }
    }
}
