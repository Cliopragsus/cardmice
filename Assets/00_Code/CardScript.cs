﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardScript : MonoBehaviour
{
    public DeckMaster.Card card;
    public DeckMaster.ExecutionLoop referencedCardDelegate;

    public enum PickState { Slotted, Held };
    public PickState pickState;

    public enum UseEffect { None, Burn, Hurt }
    public UseEffect useEffect;

    public Vector2 lastPos;

    // Update is called once per frame
    void Update()
    {
        // count down special conditions
        switch (pickState)
        {
            case PickState.Slotted:
                break;
            case PickState.Held:
                transform.position = Input.mousePosition;
                gameObject.layer = 2;
                break;
            default:
                break;
        }
    }

    public void Init()
    {
        gameObject.name = "Card_ " + card.ToString();
        transform.Find("Text").GetComponent<Text>().text = card.ToString();
        switch (card)
        {
            case DeckMaster.Card.BasicAttack:
                referencedCardDelegate = DeckMaster.instance.BasicAttack;
                break;
            case DeckMaster.Card.Dash:
                referencedCardDelegate = DeckMaster.instance.Dash;
                break;
            case DeckMaster.Card.RageFunnel:
                referencedCardDelegate = DeckMaster.instance.RageFunnel;
                break;
            case DeckMaster.Card.FireBall:
                referencedCardDelegate = DeckMaster.instance.FireBall;
                //useEffect = UseEffect.Burn;
                break;
            default:
                break;
        }
    }

    public void Drop()
    {
        pickState = PickState.Slotted;
        transform.position = lastPos;
        gameObject.tag = "Slotted";
        Debug.Log("Drop()");
    }

    public void PickUp()
    {
        pickState = PickState.Held;
        gameObject.tag = "Held";
        lastPos = transform.position;
        Debug.Log("PickUp()");
    }

    public void SlotIn()
    {
        pickState = PickState.Slotted;
        gameObject.tag = "Slotted";
        Debug.Log("SlotIn()");
    }
}
