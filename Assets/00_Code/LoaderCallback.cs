﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoaderCallback : MonoBehaviour
{
    public bool isFirstUpdate = true;
    private float count;

    public Sprite[] screens;
    public GameObject image;
    public Image img;

    private void Start()
    {
        Time.timeScale = 1;
        img = image.GetComponent<Image>();
        if (screens.Length != 0)
        {
            img.sprite = screens[Random.Range(0, screens.Length)];
        }
    }

    private void Update()
    {
        count += Time.deltaTime;
        //Debug.Log("First Update of LoaderCallback");
        if (isFirstUpdate && count >= 1)
        {
            isFirstUpdate = false;
            Loader.LoaderCallback();
        }
    }
}
