﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleRotateAround : MonoBehaviour
{
    public GameObject centerObject;
    public float speed = 1;
    Vector3 axis = new Vector3(0, 1, 0);

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(centerObject.transform.position, axis, speed * Time.deltaTime);
    }
}
