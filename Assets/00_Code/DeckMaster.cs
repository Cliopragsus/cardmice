﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DeckMaster : MonoBehaviour
{
    public static DeckMaster instance;

    [Header("Card Specifics")] ///////////////////////////////////////

    public float rageFunnelDuration = 1;
    public float dash_castTime = 0.25f;
    public float fireball_castTime = 0.6f;


    [Header("Board")] ////////////////////////////////////////////////
    
    public List<GameObject> cardPointers = new List<GameObject>();
    public List<GameObject> cardPointersHUD = new List<GameObject>();
    public List<GameObject> frames = new List<GameObject>();
    public List<GameObject> framesHUD = new List<GameObject>();
    public List<Vector2> centerPositions = new List<Vector2>();
    public List<List<GameObject>> decks = new List<List<GameObject>>();
    public List<List<GameObject>> decksHUD = new List<List<GameObject>>();
    public List<List<GameObject>> slots = new List<List<GameObject>>();
    public List<List<GameObject>> slotsHUD = new List<List<GameObject>>();
    public List<int> deckIndeces = new List<int>();

    public List<GameObject> hand = new List<GameObject>(6);
    public List<GameObject> handSlots = new List<GameObject>(6);


    public GameObject heldCard;
    public GameObject pickUpResult;
    public GameObject dropResult;

    public enum CardCreationTarget { InHand, Deck };
    public enum FrameCreationTarget { Player, Ally, Enemy };

    public GameObject hud;
    public GameObject board;
    public GameObject canvas;
    public GameObject eventSystem;
    public GameObject allyFrames;
    public GameObject enemyFrames;
    [HideInInspector] public EventSystem es;
    [HideInInspector] public PointerEventData pde;
    [HideInInspector] public GraphicRaycaster gr;
    [HideInInspector] public List<RaycastResult> rcResults = new List<RaycastResult>();


    [Header("Other")] ////////////////////////////////////////////////

    public List<GameObject> activeCharacters;

    public delegate void ExecutionLoop(CombatIntelligence user);

    [SerializeField]
    public enum Frame { Player_00_Basic, Ally_00_Basic, BaseEnemy };

    [SerializeField]
    public enum Card { BasicAttack, Dash, FireBall, RageFunnel};
    public Card card;

    public enum CardState { Executing, Idle };
    public CardState playerCardState;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.currentGameState == GameManager.GameState.InGame)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                CreateCard(Card.FireBall, CardCreationTarget.InHand);
            }

            if (Input.GetKeyDown(KeyCode.Tab))
            {
                ToggleBoard("on");
            }
            if (Input.GetKeyUp(KeyCode.Tab))
            {
                ToggleBoard("off");
            }

            if (board.activeSelf)
            {
                if ((Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.V)) && heldCard == null) 
                    // check if you hit a slotted card and make it heldCard
                {
                    UI_Check();
                    foreach (RaycastResult result in rcResults)
                    {
                        if (result.gameObject.tag == "Slotted")
                        {
                            result.gameObject.GetComponent<CardScript>().PickUp();
                            heldCard = result.gameObject;
                            for (int i = 0; i < decks.Count; i++)
                            {
                                if (decks[i].Contains(result.gameObject))
                                // remove the card from the corresponding list
                                // and clean up the corresponding hud element
                                {
                                    for (int o = 0; o < decks[i].Count; o++)
                                    {
                                        if (decks[i][o] == result.gameObject)
                                        {
                                            decks[i][o] = null;
                                            Destroy(decksHUD[i][o]);
                                            decksHUD[i][o] = null;
                                            pickUpResult = result.gameObject;
                                        }
                                    }
                                }
                            }

                            if (hand.Contains(result.gameObject))
                            {
                                for (int i = 0; i < handSlots.Count; i++)
                                {
                                    if (hand[i] == result.gameObject)
                                    {
                                        hand[i] = null;
                                        pickUpResult = result.gameObject;
                                    }
                                }
                            }
                        }
                    }
                }
                if ((Input.GetMouseButtonUp(0) || Input.GetKeyUp(KeyCode.V)) && heldCard != null)
                {
                    // while having a heldCard, check where to put it upon release
                    UI_Check();
                    foreach (RaycastResult result in rcResults)
                    {
                        if (result.gameObject.tag == "Card Slot")
                        {
                            //Debug.Log("Hit a slot: " + result.gameObject.name);
                            for (int i = 0; i < slots.Count; i++)
                            {
                                if (slots[i].Contains(result.gameObject)) // check which deck the slot belongs to
                                {
                                    for (int o = 0; o < slots[i].Count; o++)
                                    {
                                        //Debug.Log("slot test: " + slots[i][o].name);
                                        if (slots[i][o] == result.gameObject)
                                        {
                                            //Debug.Log("found result slot");
                                            if (decks[i][o] != null) // if slot is occupied, move the occupying card to hand
                                            {
                                                //Debug.Log("Slot occupied. Moving old card to hand");
                                                for (int p = 0; p < handSlots.Count; p++)
                                                {
                                                    if (hand[p] == null || p == 5)
                                                    {
                                                        if (hand[p] != null)
                                                        {
                                                            Destroy(hand[p]);
                                                            hand[p] = null;
                                                        }
                                                        hand[p] = decks[i][o];
                                                        decks[i][o].GetComponent<CardScript>().SlotIn();
                                                        decks[i][o].transform.position = handSlots[p].transform.position;
                                                        p = handSlots.Count + 1;
                                                        Destroy(decksHUD[i][o]);
                                                        decksHUD[i][o] = null;
                                                    }
                                                }
                                            }
                                            // then place heldCard into the deck onto the corresponding slot position
                                            // and create a clone for the corresponding hud
                                            decks[i][o] = heldCard;
                                            CreateHUDClone(heldCard.GetComponent<CardScript>().card, i, o);
                                            heldCard.GetComponent<CardScript>().SlotIn();
                                            heldCard.transform.position = result.gameObject.transform.position;
                                            heldCard = null;
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (heldCard != null)
                    {
                        heldCard.GetComponent<CardScript>().Drop();
                        heldCard = null;
                    }
                }
            }
        }
    }

    public void ChangeCardState(CardState targetExecutionLoop, CardState desiredState) // 
    {
        targetExecutionLoop = desiredState;
    }

    public GameObject CreateCard(Card type, CardCreationTarget destination) // creates a new card from prefab in the player hand area on canvas
    {
        GameObject cardGO = Instantiate(PrefabMaster.instance.baseCard, canvas.transform, false);
        switch (destination)
        {
            case CardCreationTarget.InHand:
                for (int i = 0; i < handSlots.Count; i++)
                {
                    if (hand[i] == null)
                    {
                        cardGO.transform.SetParent(board.transform, false);
                        cardGO.GetComponent<CardScript>().card = type;
                        hand[i] = cardGO;
                        
                        cardGO.GetComponent<CardScript>().SlotIn();
                        cardGO.transform.position = handSlots[i].transform.position;
                        i = handSlots.Count + 1;
                        cardGO.GetComponent<CardScript>().Init();
                        return cardGO;
                    }
                }
                break;
            case CardCreationTarget.Deck:
                cardGO.transform.SetParent(canvas.transform, false);
                cardGO.GetComponent<CardScript>().card = type;
                cardGO.GetComponent<CardScript>().Init();
                return cardGO;
        }
        return null;
    }

    public void CreateHUDClone(Card type, int decksIndex, int cardIndex)
    {
        GameObject cardGO = Instantiate(PrefabMaster.instance.baseCard, canvas.transform, false);
        cardGO.transform.SetParent(slotsHUD[decksIndex][cardIndex].transform, false);
        cardGO.GetComponent<CardScript>().card = type;
        cardGO.GetComponent<CardScript>().Init();
        decksHUD[decksIndex][cardIndex] = cardGO;
        cardGO.transform.position = slotsHUD[decksIndex][cardIndex].transform.position;
    }

    public void CreateFrame(Frame type, int unitIndex, FrameCreationTarget fct) 
        // creates frames for each unit
        // depending on the type of unit, the frame will be added to the respective canvas
        // Determine FrameType > Instantiate Frames > Create a pointer for it > Add children of frames to slots > Add cards from save or default to decks
        // simulateously create HUD clones for the respective HUD version of each step
    {
        deckIndeces.Add(0); // the index to keep track of which card is currently selected in the deck (what the pointer points at)
        Debug.Log("CreateFrame(): Creating frame of type " + type + " for unit of Index " + unitIndex + "(" + CombatManager.instance.units[unitIndex].name + ")");
        GameObject frame = null;
        GameObject frameHUD = null;
        switch (type)
        {
            case Frame.Player_00_Basic:
                frame = PrefabMaster.instance.frame_player_00_basic;
                frameHUD = frame;
                break;
            case Frame.Ally_00_Basic:
                frame = PrefabMaster.instance.frame_ally_00_basic;
                frameHUD = frame;
                break;
            case Frame.BaseEnemy:
                frame = PrefabMaster.instance.frame_baseEnemy;
                frameHUD = frame;
                break;
            default:
                Debug.LogError("CreateFrame ERROR: No frame of " + type.ToString() + " exists in constructor. Cannot create frame");
                return;
        }

        frame = Instantiate(frame, board.transform, false);
        frames.Add(frame);
        cardPointers.Add(Instantiate(PrefabMaster.instance.cardPointer, frames[unitIndex].transform, false));

        frameHUD = Instantiate(frameHUD, board.transform, false);
        framesHUD.Add(frameHUD);
        cardPointersHUD.Add(Instantiate(PrefabMaster.instance.cardPointer, framesHUD[unitIndex].transform, false));

        GameObject personalCanvas = null; // required for non player units
        switch (fct) // places the frame on the corresponding canvas
        {
            case FrameCreationTarget.Player:
                framesHUD[unitIndex].transform.SetParent(hud.transform);
                framesHUD[unitIndex].transform.localPosition = new Vector3(700, 0, 0);
                frames[unitIndex].transform.localPosition = centerPositions[unitIndex];
                break;
            case FrameCreationTarget.Ally:
                personalCanvas = CombatManager.instance.units[unitIndex].transform.Find("Personal Canvas").gameObject;
                framesHUD[unitIndex].transform.SetParent(personalCanvas.transform, false);
                frames[unitIndex].transform.localPosition = centerPositions[unitIndex];
                framesHUD[unitIndex].transform.localPosition = centerPositions[0];
                framesHUD[unitIndex].transform.Rotate(0, 180, 0);
                break;
            case FrameCreationTarget.Enemy:
                personalCanvas = CombatManager.instance.units[unitIndex].transform.Find("Personal Canvas").gameObject;
                framesHUD[unitIndex].transform.SetParent(personalCanvas.transform, false);
                framesHUD[unitIndex].SetActive(false);
                frames[unitIndex].SetActive(false);
                break;
        }

        decks.Add(new List<GameObject>());
        decksHUD.Add(new List<GameObject>());
        slots.Add(new List<GameObject>());
        slotsHUD.Add(new List<GameObject>());

        // add the slots of that frame to the slots list and the respective HUD list
        Debug.Log("CreateFrame(): Adding frameChildren of " + frame.name + "to slots");
        for (int i = 0; i < frames[unitIndex].transform.childCount - 1; i++) // - 1 because there is already the pointer added to the frame
        {
            slots[unitIndex].Add(frames[unitIndex].transform.GetChild(i).transform.gameObject);
            slotsHUD[unitIndex].Add(framesHUD[unitIndex].transform.GetChild(i).transform.gameObject);
            decks[unitIndex].Add(null);
            decksHUD[unitIndex].Add(null);
        }

        // adds the cards that are either taken from save or from GetDefaultDeck() and fills the empty List in decks[unitIndex] with them
        // then creates a clone of that card to the corresponding HUD on either the HUD object on canvas, or the respective personal canvas of the unit
        // also kinda throws the non-HUD cards on the board to be played with
        switch (fct)
        {
            case FrameCreationTarget.Player:
            case FrameCreationTarget.Ally:
                for (int p = 0; p < Loader.currentSaveState.socketedCards[unitIndex].Count; p++)
                {
                    decks[unitIndex][p] = CreateCard(Loader.currentSaveState.socketedCards[unitIndex][p], CardCreationTarget.Deck);
                    decks[unitIndex][p].transform.SetParent(board.transform, false);
                    decks[unitIndex][p].transform.position = slots[unitIndex][p].transform.position;
                    decks[unitIndex][p].GetComponent<CardScript>().SlotIn();
                    CreateHUDClone(decks[unitIndex][p].GetComponent<CardScript>().card, unitIndex, p);
                    Debug.Log("Created card and HUDclone " + type.ToString() + "for unitID " + unitIndex);
                }
                break;
            case FrameCreationTarget.Enemy: // identical progress to player and ally, only that it gets the cards from GetDefaultDeck()
                Debug.Log("Creating Default Deck for Enemy");
                List<Card> newCards = GetDefaultDeck(type);
                for (int i = 0; i < newCards.Count; i++)
                {
                    decks[unitIndex][i] = CreateCard(newCards[i], CardCreationTarget.Deck);
                    decks[unitIndex][i].transform.SetParent(enemyFrames.transform, false);
                    decks[unitIndex][i].transform.position = slots[unitIndex][i].transform.position;
                    decks[unitIndex][i].GetComponent<CardScript>().SlotIn();
                    CreateHUDClone(decks[unitIndex][i].GetComponent<CardScript>().card, unitIndex, i);
                    Debug.Log("Created card and HUDclone " + type.ToString() + "for unitID " + unitIndex);
                }
                break;
            default:
                break;
        }
    }

    public List<Card> GetDefaultDeck(Frame desiredDeck)
    {
        List<Card> cardList = new List<Card>();
        switch (desiredDeck)
        {
            case Frame.Player_00_Basic:
                cardList.Add(Card.Dash);
                cardList.Add(Card.RageFunnel);
                break;
            case Frame.Ally_00_Basic:
                cardList.Add(Card.Dash);
                cardList.Add(Card.Dash);
                cardList.Add(Card.BasicAttack);
                break;
            case Frame.BaseEnemy:
                cardList.Add(Card.BasicAttack);
                break;
            default:
                break;
        }
        return cardList;
    }

    public void IncreaseDeckIndex(int deckIndex) // takes an int to increase the corresponding decks index
    {
        deckIndeces[deckIndex]++;
        if (deckIndeces[deckIndex] >= decks[deckIndex].Count)
        {
            deckIndeces[deckIndex] = 0;
        }
        cardPointers[deckIndex].transform.position = slots[deckIndex][deckIndeces[deckIndex]].transform.position;
        cardPointersHUD[deckIndex].transform.position = slotsHUD[deckIndex][deckIndeces[deckIndex]].transform.position;
    }

    public void Init() // sets up frames and loads them with cards through CreateCard
    {
        canvas = GameObject.Find("Canvas").transform.gameObject;
        board = canvas.transform.Find("Board").transform.gameObject;
        hud = canvas.transform.Find("HUD").transform.gameObject;
        gr = canvas.GetComponent<GraphicRaycaster>();
        es = GameObject.Find("EventSystem").transform.gameObject.GetComponent<EventSystem>();

        allyFrames = board.transform.Find("Area_Allies").gameObject;
        enemyFrames = board.transform.Find("Area_Enemies").gameObject;
        hand = new List<GameObject>(6);
        GameObject handArea = board.transform.Find("Area_Hand").gameObject;
        for (int i = 0; i < 6; i++)
        {
            hand.Add(null);
            handSlots.Add(handArea.transform.GetChild(i).gameObject);
        }
    }

    public void SlotIn(GameObject card, int unitIndex, int deckIndex)
    {
        /// to do:
        /// cleanout target location
        /// '-> put possible old card into hand
        /// update deck
        /// update hud deck
        /// 

        
    }

    public void SlotOut(GameObject card, GameObject target)
    {
        // cleanout target
    }

    public void UseCard(CombatIntelligence user) // called by units to ask for next card. Determines the card and updates the units delegate to use the corresponding decks ability
    {
        if (user.currentCardState == CombatIntelligence.CardState.Idle)
        {
            for (int i = 0; i < decks[user.id].Count; i++)
            {
                if (decks[user.id][deckIndeces[user.id]] != null)
                {
                    Debug.Log("Unit " + user.id + " uses card: " + decks[user.id][deckIndeces[user.id]].name);
                    GameObject pickedCard = decks[user.id][deckIndeces[user.id]];
                    user.currentCardState = CombatIntelligence.CardState.Executing;
                    user.executionLoop = pickedCard.GetComponent<CardScript>().referencedCardDelegate;
                    if (pickedCard.GetComponent<CardScript>().useEffect == CardScript.UseEffect.Burn)
                    {
                        Destroy(pickedCard);
                        decks[user.id][deckIndeces[user.id]] = null;
                        Destroy(decksHUD[user.id][deckIndeces[user.id]].gameObject);
                        decks[user.id][deckIndeces[user.id]] = null;
                    }
                    IncreaseDeckIndex(user.id);
                    break;
                }
                else
                {
                    IncreaseDeckIndex(user.id);
                }
            }
            ChangeCardState(playerCardState, CardState.Executing);
        }
    }

    public void ToggleBoard(string s) // toggles the card board and limits the live game
    {
        if (GameManager.instance.currentGameState != GameManager.GameState.Paused)
        {
            switch (s)
            {
                case "on":
                    board.SetActive(true);
                    //Time.timeScale = 0.1f;
                    //GameManager.instance.ingameTimeScale = 0.1f;
                    //Cursor.lockState = CursorLockMode.None;
                    Cursor.visible = true;
                    hud.SetActive(false);
                    break;
                case "off":
                    board.SetActive(false);
                    //Time.timeScale = 1;
                    //GameManager.instance.ingameTimeScale = 1;
                    //Cursor.lockState = CursorLockMode.Locked;
                    //Cursor.visible = false;
                    //hud.SetActive(true);
                    break;
                default:
                    break;
            }
        }
    }

    public void UI_Check() // updates the list of Ui elements under mouseposition
    {
        pde = new PointerEventData(es);
        pde.position = Input.mousePosition;
        rcResults = new List<RaycastResult>();
        gr.Raycast(pde, rcResults);
    }


    /// <summary>
    //////////////////////////////////////////////////////////////////// CARD ABILITES ///////////////////////////////////////////////////////////////////////////
    /// </summary>
    /// 

    public void EXAMPLE_FOR_ABILITY(CombatIntelligence user)
    {
        // needs to define a range that the unit is bound to achieve first
        // if the range is not enough, the user will loop into a move state, then return to this ability
        // this range check is only for non player units, the range check will always return true for the player

        // TO DO : create a bool for temp objects to be destroyed (or not) if the units state is changed
        user.desiredRange = 5;
        if (!user.InRange(user.desiredRange))
        {
            user.Request_ActionState(CombatIntelligence.ActionState.MoveIntoRange);
            return;
        }

        // needs to start something on user.executionCounter == 0
        if (user.executionCounter == 0)
        {
            // create a temporary object as user.executionTempObject
            // access the object through the prefabmaster and then do whatever is needed with it
            user.executionTempObject = Instantiate(PrefabMaster.instance.fireballHitBox, user.transform);

            // determine whether it should be destroyed if the player is stunned for example (as the dash hitbox should, but the fireball shouldnt)
            user.executionTempObject_isPersistent = true;
            user.executionTempObject.GetComponent<Fireball>().Connect(user.gameObject);
            user.nma.speed = 1000;
        }

        // have the ability run its time through executionCounter
        user.executionCounter += Time.deltaTime;

        // check for castTimes etc. these should be their own variable under the Ability Specifics section 
        // once all conditions have been met, reverse all changes to the user that you have done in this void, and deactivate the user
        if (user.executionCounter >= fireball_castTime)
        {
            user.executionTempObject.GetComponent<Fireball>().SetMoveState(Fireball.MoveState.Moving);
            user.Deactivate();
            user.nma.speed = 3.5f;
        }
    }










    public void BasicAttack(CombatIntelligence user)
    {
        user.desiredRange = user.attackRange;
        if (!user.InRange(user.desiredRange))
        {
            user.Request_ActionState(CombatIntelligence.ActionState.MoveIntoRange);
            return;
        }
        user.Request_ActionState(CombatIntelligence.ActionState.Attacking);
    }

    public void Dash(CombatIntelligence user) // moves the player forward with a damaging hitbox
    {
        user.desiredRange = 5;
        if (Vector3.Distance(user.gameObject.transform.position, user.target_GO.transform.position) > user.desiredRange)
        {
            user.Request_ActionState(CombatIntelligence.ActionState.MoveIntoRange);
            return;
        }
        if (user.executionCounter == 0)
        {
            user.executionTempObject_isPersistent = false;
            user.executionTempObject = Instantiate(PrefabMaster.instance.dashHitBox, user.gameObject.transform.position + user.gameObject.transform.forward + Vector3.up, user.gameObject.transform.rotation);
            user.executionTempObject.transform.parent = user.gameObject.transform;
            user.executionTempObject.GetComponent<DashHitBox>().Connect();
            if (user.gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>())
            {
                user.gameObject.transform.LookAt(user.target_GO.transform.position);
                user.nma.destination = user.target_GO.transform.position;
            }
        }

        user.executionCounter += Time.deltaTime;

        if (user.executionCounter >= dash_castTime)
        {
            user.anim.Play("Dash");
            if (user.gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>())
            {
                user.nma.angularSpeed = 40;
            }
            user.transform.Translate(Vector3.forward * 10 * Time.deltaTime);
        }
        if (user.executionCounter >= (0.6f + dash_castTime))
        {
            user.anim.Play(("Idle"));
            Destroy(user.executionTempObject);
            user.executionCounter = 0;
            user.Deactivate();
        }
    }

    public void FireBall(CombatIntelligence user) // channels a fireball projectile
    {
        if (user.executionCounter == 0)
        {
            user.anim.Play("Cast");
            user.nma.destination = user.transform.position;
            user.executionTempObject = Instantiate(PrefabMaster.instance.fireballHitBox, user.gameObject.transform.position + user.gameObject.transform.forward + Vector3.up, Quaternion.identity);
            user.executionTempObject.GetComponent<Fireball>().Connect(user.gameObject);
            user.executionTempObject_isPersistent = true;
            if (user.gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>())
            {
                user.nma.angularSpeed = 360;
            }
        }
        user.executionCounter += Time.deltaTime;
        if (user.executionCounter >= fireball_castTime)
        {
            if (user.gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>())
            {
                user.nma.angularSpeed = 120;
            }
            user.executionTempObject.GetComponent<Fireball>().SetMoveState(Fireball.MoveState.Moving);
            user.executionCounter = 0;
            user.executionTempObject = null;
            user.Deactivate();
        }
    }

    public void RageFunnel(CombatIntelligence user) // sets the unit in a channel state that can create a fireball card in RageFunnel (script on created hitbox)
    {
        if (user.executionCounter == 0)
        {
            user.anim.Play("Counter");
            user.executionTempObject = Instantiate(PrefabMaster.instance.rageFunnelHitBox, user.gameObject.transform.position + Vector3.up, user.gameObject.transform.rotation);
            user.executionTempObject_isPersistent = false;
            user.executionTempObject.transform.parent = user.gameObject.transform;
            user.executionTempObject.GetComponent<RageFunnel>().Connect();
            user.currentHurtState = CombatIntelligence.HurtState.Invincible;
        }
        user.executionCounter += Time.deltaTime;
        user.moveSpeed = 0.5f;
        if (user.executionCounter >= rageFunnelDuration)
        {
            user.currentHurtState = CombatIntelligence.HurtState.Vurnerable;
            user.executionCounter = 0;
            Destroy(user.executionTempObject);
            user.executionTempObject = null;
            user.moveSpeed = 2;
            user.Deactivate();
        }
    }
}
