﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabMaster : MonoBehaviour
{
    [Header("Base Elements")]
    public GameObject baseCard;

    [Header("Card Frames")]
    public GameObject cardPointer;
    public GameObject frame_player_00_basic;
    public GameObject frame_ally_00_basic;
    public GameObject frame_baseEnemy;

    [Header("Hitboxes")]
    public GameObject playerAutoHitBox;
    public GameObject dashHitBox;
    public GameObject rageFunnelHitBox;
    public GameObject fireballHitBox;

    [Header("Materials")]
    public Material dissolve_red;

    [Header("Organisation")]
    public GameObject pauseMenu;

    [Header("Units")]
    public GameObject player;
    public GameObject magnus;
    public GameObject baseEnemy;



    public static PrefabMaster instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }
}
