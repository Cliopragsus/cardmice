﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatManager : MonoBehaviour
{

    public enum Affilitation { None, Ally, Enemy };
    public static CombatManager instance;

    public List<GameObject> units;
    public enum UnitType { Player, Magnus, BaseEnemy };

    private GameObject tempNewTarget;

    private PrefabMaster p;

    public CombatIntelligence selectedUnit;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        p = PrefabMaster.instance;
    }

    public void Update()
    {
        Command();
    }

    public void Command()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;

        //////////////////////////////////////////////                 Mouse 1  |   Move Queue      //////////////////////////////////

        // get a unit that you want to influence, specifically allied units
        if (Input.GetMouseButtonDown(0))
        {
            if (selectedUnit == null)
            {
                if (Physics.Raycast(ray, out hitInfo, 100))
                {
                    if (hitInfo.collider.gameObject.GetComponent<CombatIntelligence>())
                    {
                        CombatIntelligence unit = hitInfo.collider.gameObject.GetComponent<CombatIntelligence>();
                        if (unit.affiliation == Affilitation.Ally)
                        {
                            selectedUnit = unit;
                        }
                    }
                }
            }
        }

        // to do: during get, update a marker for the next action and suspend the units action? (as in: after finishing the current action, it will wait, and queue otherwise)

        // depending on the result of the release of the mousebutton, update the next moveposition
        if (Input.GetMouseButtonUp(0) && selectedUnit != null && !Input.GetKey(KeyCode.LeftShift))
        {
            if (Physics.Raycast(ray, out hitInfo, 100))
            {
                selectedUnit.nma.destination = hitInfo.point;
                selectedUnit.Request_ActionState(CombatIntelligence.ActionState.MoveToDestination);
            }
            selectedUnit = null;
        }
        if (Input.GetMouseButtonUp(0) && selectedUnit != null && Input.GetKey(KeyCode.LeftShift))
        {
            if (Physics.Raycast(ray, out hitInfo, 100))
            {
                Debug.LogWarning("Queued Destination");
                selectedUnit.QueueAction(null, hitInfo.point);
                if (selectedUnit.currentActionState == CombatIntelligence.ActionState.Idle)
                {
                    selectedUnit.Request_ActionState(CombatIntelligence.ActionState.CheckQueue);
                }
            }
        }

        ///////////////////////////////////////////////            Mouse 2    |    Card Queue              ///////////////////////////////////
        ///
        if (Input.GetMouseButtonDown(1))
        {
            if (selectedUnit == null)
            {
                if (Physics.Raycast(ray, out hitInfo, 100))
                {
                    if (hitInfo.collider.gameObject.GetComponent<CombatIntelligence>())
                    {
                        CombatIntelligence unit = hitInfo.collider.gameObject.GetComponent<CombatIntelligence>();
                        if (unit.affiliation == Affilitation.Ally)
                        {
                            selectedUnit = unit;
                        }
                    }
                }
            }
        }

        // to do: targeter for cardTarget, highlight card affected as well as freezing unit action if it finished casting during getMousebutton(1)

        if (Input.GetMouseButtonUp(1) && selectedUnit != null && !Input.GetKeyDown(KeyCode.LeftShift))
        {
            if (Physics.Raycast(ray, out hitInfo, 100))
            {
                if (hitInfo.collider.gameObject.GetComponent<CombatIntelligence>())
                {
                    CombatIntelligence unit = hitInfo.collider.gameObject.GetComponent<CombatIntelligence>();
                    if (unit.affiliation == Affilitation.Enemy)
                    {
                        selectedUnit.target_GO = unit.gameObject;
                    }
                }
            }
            selectedUnit = null;
        }
        if (Input.GetMouseButtonUp(1) && selectedUnit != null && Input.GetKeyDown(KeyCode.LeftShift))
        {
            if (Physics.Raycast(ray, out hitInfo, 100))
            {
                if (hitInfo.collider.gameObject.GetComponent<CombatIntelligence>())
                {
                    CombatIntelligence unit = hitInfo.collider.gameObject.GetComponent<CombatIntelligence>();
                    if (unit.affiliation == Affilitation.Enemy)
                    {
                        selectedUnit.QueueAction(unit, Vector3.zero);
                    }
                }
            }
        }

        /////////////////////////////////////       Shift        /////////////////////////////
        
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            selectedUnit = null;
        }
    }

    public void CreateNewUnit(UnitType type, Vector3 spawnPoint)
    {
        Loader.SaveState s = Loader.currentSaveState;

        GameObject newUnit = null;
        DeckMaster.Frame newFrame = DeckMaster.Frame.Player_00_Basic;
        DeckMaster.FrameCreationTarget fct = new DeckMaster.FrameCreationTarget();
        switch (type)
        {
            case UnitType.Player:
                newUnit = p.player;
                newFrame = s.frame_player;
                fct = DeckMaster.FrameCreationTarget.Player;
                break;
            case UnitType.Magnus:
                newUnit = p.magnus;
                newFrame = s.frame_ally0;
                fct = DeckMaster.FrameCreationTarget.Ally;
                break;
            case UnitType.BaseEnemy:
                newUnit = p.baseEnemy;
                newFrame = DeckMaster.Frame.BaseEnemy;
                fct = DeckMaster.FrameCreationTarget.Enemy;
                break;
            default:
                break;
        }
        newUnit = Instantiate(newUnit, spawnPoint, Quaternion.identity);
        newUnit.GetComponent<CombatIntelligence>().id = units.Count;
        units.Add(newUnit);
        DeckMaster.instance.CreateFrame(newFrame, newUnit.GetComponent<CombatIntelligence>().id, fct);
    }

    public void Init()
    {
        Debug.Log("CombatManager Init");
        Vector3 defaultSpawn = GameObject.Find("DefaultSpawnPoint").transform.position;
        units = new List<GameObject>();
        CreateNewUnit(UnitType.Player, defaultSpawn);
        if (Loader.currentSaveState.allyOne == true)
        {
            CreateNewUnit(UnitType.Magnus, defaultSpawn);
        }
    }

    public void Register(GameObject newUnit)
    {
        newUnit.GetComponent<CombatIntelligence>().id = units.Count;
        units.Add(newUnit);
    }

    public GameObject RequestNewTarget(GameObject receiver)
    {
        tempNewTarget = null;
        foreach(GameObject potentialTarget in units)
        {
            if (potentialTarget != null)
            {
                if (potentialTarget.GetComponent<CombatIntelligence>().affiliation != receiver.GetComponent<CombatIntelligence>().affiliation
                && potentialTarget.GetComponent<CombatIntelligence>().currentActionState != CombatIntelligence.ActionState.Dead)
                {
                    if (tempNewTarget == null)
                    {
                        tempNewTarget = potentialTarget;
                    }
                    else if (Vector3.Distance(receiver.transform.position, potentialTarget.transform.position) < Vector3.Distance(receiver.transform.position, tempNewTarget.transform.position))
                    {
                        tempNewTarget = potentialTarget;
                    }
                }
            }
        }
        if (tempNewTarget != null)
        {
            Debug.Log("found " + tempNewTarget.name + " as new target");
            return tempNewTarget;
        }
        else
        {
            //Debug.Log(receiver.name + ": no legal target found");
            receiver.GetComponent<CombatIntelligence>().Deactivate();
            receiver.GetComponent<CombatIntelligence>().Request_ActionState(CombatIntelligence.ActionState.Idle);
            return null;
        }
    }
}
