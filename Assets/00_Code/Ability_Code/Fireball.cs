﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    public CombatManager.Affilitation affiliation;
    public CombatIntelligence parentCI;
    public string hitBoxName;
    public int damage;

    public Vector3 destination;
    public float moveSpeed;
    public float explosionRadius = 3;

    public GameObject explosionFX;

    public enum MoveState { Idle, Moving };
    public MoveState moveState;

    public void Connect(GameObject parent)
    {
        parentCI = parent.GetComponent<CombatIntelligence>();
        affiliation = parentCI.affiliation;
        damage = parentCI.attackDamage * 3;
        GetComponent<Collider>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        switch (moveState)
        {
            case MoveState.Idle:
                break;
            case MoveState.Moving:
                transform.position = Vector3.MoveTowards(transform.position, destination, moveSpeed * Time.deltaTime);
                GetComponent<Collider>().enabled = true;
                break;
        }
        if (Vector3.Distance(transform.position, destination) < 0.1f)
        {
            Explode();
        }
    }

    public void Explode()
    {
        Instantiate(explosionFX, transform.position, Quaternion.identity);

        RaycastHit[] hits = Physics.SphereCastAll(transform.position, explosionRadius, Vector3.up, 0.1f);

        switch (affiliation)
        {
            case CombatManager.Affilitation.None:
                break;
            case CombatManager.Affilitation.Ally:
                foreach(RaycastHit hit in hits)
                {
                    if (hit.collider.gameObject.GetComponent<CombatIntelligence>())
                    {
                        CombatIntelligence enemy = hit.collider.gameObject.GetComponent<CombatIntelligence>();
                        if (enemy.affiliation == CombatManager.Affilitation.Enemy)
                        {
                            enemy.damageDelegate(damage);
                        }
                    }
                }
                break;
            case CombatManager.Affilitation.Enemy:
                foreach (RaycastHit hit in hits)
                {
                    if (hit.collider.gameObject.GetComponent<CombatIntelligence>())
                    {
                        CombatIntelligence ally = hit.collider.gameObject.GetComponent<CombatIntelligence>();
                        if (ally.affiliation == CombatManager.Affilitation.Ally)
                        {
                            ally.damageDelegate(damage);
                        }
                    }
                }
                break;
            default:
                Debug.LogWarning(gameObject.name + "did not receive affiliation");
                break;
        }

        Destroy(gameObject);
    }

    public void SetMoveState(MoveState desiredState)
    {
        switch (desiredState)
        {
            case MoveState.Idle:
                moveState = MoveState.Idle;
                break;
            case MoveState.Moving:
                if (parentCI.target_GO != null)
                {
                    destination = parentCI.target_GO.transform.position;
                }
                else if (parentCI.target_Vec3 != null)
                {
                    destination = parentCI.target_Vec3;
                }
                else
                {
                    destination = parentCI.gameObject.transform.forward * 10;
                }
                moveState = MoveState.Moving;
                break;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<CombatIntelligence>())
        {
            if (other.GetComponent<CombatIntelligence>().affiliation != affiliation)
            {
                if (moveState == MoveState.Moving)
                {
                    Explode();
                }
            }
        }
    }
}
