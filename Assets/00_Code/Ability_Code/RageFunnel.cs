﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RageFunnel : MonoBehaviour
{
    public CombatIntelligence parentCI;
    public int funnelCount = 0;
    public int funnelThreshold = 1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Connect()
    {
        parentCI = transform.parent.gameObject.GetComponent<CombatIntelligence>();
        parentCI.damageDelegate += Conversion;
    }

    public void Conversion(int proxy)
    {
        funnelCount++;
        if (funnelCount >= funnelThreshold)
        {
            parentCI.damageDelegate -= Conversion;
            DeckMaster.instance.CreateCard(DeckMaster.Card.FireBall, DeckMaster.CardCreationTarget.InHand);
            Debug.Log("Funnel Success");
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        // create Particle
    }
}
