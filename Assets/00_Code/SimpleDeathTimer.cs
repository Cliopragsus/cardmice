﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleDeathTimer : MonoBehaviour
{
    public float timeLimit = 2;
    public float timeCount;

    // Start is called before the first frame update
    void Start()
    {
        timeCount = timeLimit;
    }

    // Update is called once per frame
    void Update()
    {
        timeCount -= Time.deltaTime;
        if (timeCount <= 0)
        {
            Destroy(gameObject);
        }
    }
}
