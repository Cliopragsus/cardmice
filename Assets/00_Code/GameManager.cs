﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public enum GameState { Init, InGame, Paused, Menu };
    public GameState currentGameState = GameState.Init;
    public GameState lastGameState;

    string json;

    public float playTime;
    public float ingameTimeScale = 1;

    [HideInInspector] public GameObject pauseMenu;

    // stringConversion
    [HideInInspector]
    public string ltos;
    [HideInInspector]
    public List<List<DeckMaster.Card>> stol = new List<List<DeckMaster.Card>>();
    public string[] stolBig;
    public string[] stolSmall;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        //currentGameState = GameState.Init;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            Debug.Log(DeckMaster.instance.decks[0][0].GetComponent<CardScript>().card.ToString());
        }

        GameStateAction();

        if (currentGameState == GameState.InGame)
        {
            
            //Cursor.lockState = CursorLockMode.Locked;
        }

        if (Input.GetKeyDown(KeyCode.Escape) 
            && SceneManager.GetActiveScene().ToString() != "Start")
        {
            if (pauseMenu == null)
            {
                pauseMenu = Instantiate(PrefabMaster.instance.pauseMenu, GameObject.Find("Canvas").transform, false);
                pauseMenu.SetActive(false);
                PauseMenu_Toggle();
            }
            else if (pauseMenu != null)
            {
                PauseMenu_Toggle();
            }
        }

        GameStateAction();
    }

    public void GameStateAction()
    {
        switch (currentGameState)
        {
            case GameState.Init:
                if (Loader.currentSaveState == null)
                {
                    Debug.Log("Start Scene detected");
                    Loader.Init();
                }
                else
                {
                    Loader.Init();
                    DeckMaster.instance.Init();
                    CombatManager.instance.Init();
                }

                currentGameState = GameState.InGame;
                break;
            case GameState.InGame:
                Loader.currentPlayTime += Time.deltaTime / ingameTimeScale;
                break;
            case GameState.Paused:
                break;
            case GameState.Menu:
                break;
            default:
                break;
        }
    }

    // This function should be @ loader but was to messy to build in there since it is a static class, thus
    // is was moved here in order to keep my nerves from twisting

        // takes the list of currently socketedCards and combines them into a string that can be saved via
        // jsonUtility, since that does not support lists or arrays to be serialized

        // this will be called by the loader before saving

        // to do: organize loading related functions properly in one script
    public void ConvertListOfListToString(List<List<DeckMaster.Card>> listToConvert)
    {
        ltos = null;
        for (int i = 0; i < listToConvert.Count; i++)
        {
            for (int o = 0; o < listToConvert[i].Count; o++)
            {
                ltos += listToConvert[i][o].ToString();
                if (o == listToConvert[i].Count -1)
                {
                    if (i != (listToConvert.Count - 2))
                    {
                        ltos += ";";
                    }
                }
                else
                {
                    ltos += ",";
                }
            }
            
        }
        /*
        foreach (List<DeckMaster.Card> list in listToConvert)
        {
            foreach (DeckMaster.Card c in list)
            {
                ltos += c.ToString();
            }
            ltos += ";";
        }
        */
        Loader.currentSaveState.socketedCardsString = ltos;
        
    }

    // This function should be @ loader but was to messy to build in there since it is a static class, thus
    // is was moved here in order to keep my nerves from twisting

        // takes the string created by ConverListOfListToString and reverts it back to a list that can be used by the
        // loader, since jsonUtility does not support saving lists

        // this will be called by the loader before loading
    public void ConvertStringToListOfList(string stringToConvert)
    {
        stolBig = stringToConvert.Split(';');
        Debug.Log("stolBig now holds " + stolBig.Length + " strings after first split");
        for (int i = 0; i < stolBig.Length; i++)
        {
            stolSmall = stolBig[i].Split(',');
            Debug.Log("stolSmall holds " + stolSmall.Length + " strings for current conversion");
            stol.Add(new List<DeckMaster.Card>());
            Debug.Log("Adding new List of cards to stol (now at " + stol.Count + ") to fit current conversion");
            for (int o = 0; o < stolSmall.Length; o++)
            {
                string s = stolSmall[o];
                s.Trim(',', ';');
                Debug.Log("Trying to convert stollSmall[" + o + "] (" + s + ") to a card");
                DeckMaster.Card c = new DeckMaster.Card();
                switch (s)
                {
                    case "Dash":
                        c = DeckMaster.Card.Dash;
                        break;
                    case "RageFunnel":
                        c = DeckMaster.Card.RageFunnel;
                        break;
                    case "FireBall":
                        c = DeckMaster.Card.FireBall;
                        break;
                    case "BasicAttack":
                        c = DeckMaster.Card.BasicAttack;
                        break;
                    default:
                        Debug.LogWarning("ConversionString for socketed cards did not return a matching card. Given string: " + s);
                        break;
                }
                stol[i].Add(c);
                Debug.Log("Adding card " + c.ToString() + " to stol[" + o + "]");
            }

        }
        Loader.currentSaveState.socketedCards = stol;
    }

    /*
    public Loader.SaveState GetSave()
    {
        Loader.SaveState saveInfo = new Loader.SaveState();
        if (DeckMaster.instance.ally)
        return saveInfo;
    }
    */

    public void UpdateCurrentSave()
    {
        Loader.currentSaveState.socketedCards[0] = new List<DeckMaster.Card>();
        for (int i = 0; i < DeckMaster.instance.decks[0].Count; i++)
        {
            Loader.currentSaveState.socketedCards[0].Add(DeckMaster.instance.decks[0][i].GetComponent<CardScript>().card);
        }
        
        if (Loader.currentSaveState.allyOne)
        {
            Loader.currentSaveState.socketedCards[1] = new List<DeckMaster.Card>();
            for (int i = 0; i < DeckMaster.instance.decks[1].Count; i++)
            {
                Loader.currentSaveState.socketedCards[1].Add(DeckMaster.instance.decks[1][i].GetComponent<CardScript>().card);
            }
        }
        
    }

    public void PauseMenu_Toggle()
    {
        if(pauseMenu.activeSelf)
        {
            pauseMenu.SetActive(false);
            lastGameState = currentGameState;
            currentGameState = GameState.InGame;
            Time.timeScale = ingameTimeScale;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            pauseMenu.GetComponent<StartMenu>().ReturnToMain();
        }
        else
        {
            pauseMenu.SetActive(true);
            lastGameState = currentGameState;
            currentGameState = GameState.Paused;
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }
}
