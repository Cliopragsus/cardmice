﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CombatIntelligence : MonoBehaviour
{
    [Header("Stats")]
    public int currentHealth;
    public int maxHealth;

    public float moveSpeed;
    public float currentVerticalSpeed;
    public float nma_defaultSpeed = 3.5f;
    public float nma_defaultAngularSpeed = 120;

    public int attackDamage = 1;
    public float attackSpeed = 1;
    public float attackRange = 1;

    public float desiredRange; // used by abilities to set a minimum range for it to be used

    [HideInInspector] public int staggerCount;
    public int staggerThreshold = 1;
    [HideInInspector] public float staggerTimer;
    [HideInInspector] public float deathCountout;
    public float transmissionTimeBetweenCards = 0.3f;
    [HideInInspector] public float multiPurposeCounter;

    [Header("Stat Related")]
    [HideInInspector] public float attackCounter;
    [HideInInspector] public int attackIndex;
    [HideInInspector] public GameObject autoHitBox;
    [HideInInspector] public int attackAnimIndex;

    [Header("Objects")]
    public GameObject target_GO;
    public Vector3 target_Vec3;

    [Header("Orga")]
    public int id = 99;
    public CombatManager.Affilitation affiliation;
    public GameObject personalCanvas;

    //public delegate void ExecutionLoop(CombatIntelligence user);
    public DeckMaster.ExecutionLoop executionLoop;
    public delegate void QueueDelegate(CombatIntelligence target, Vector3 destination);
    public QueueDelegate queuedAction;
    public List<QueueDelegate> queue = new List<QueueDelegate>();
    public int queueCount;
    public List<CombatIntelligence> queue_targets;
    public List<Vector3> queue_destinations;

    [HideInInspector] public float executionCounter;
    [HideInInspector] public GameObject executionTempObject;
    [HideInInspector] public bool executionTempObject_isPersistent;

    public delegate void DamageDelegate(int damage);
    public DamageDelegate damageDelegate;

    public enum CardState { Idle, Executing };
    public CardState currentCardState;

    public enum ActionState { Attacking, CC, CheckQueue, Dead, Idle, MoveIntoRange, MoveToDestination, Special_Full,
        Special_Transmission, Staggered, Walking, };
    public ActionState currentActionState;
    [HideInInspector] public ActionState lastActionState;

    public enum HurtState { Vurnerable, Invincible };
    public HurtState currentHurtState;

    [HideInInspector] public Animator anim;
    public enum AnimState { Idle, Forward, Backwards, Left, Right, Attack, Cast, Hit };
    [HideInInspector] public AnimState currentAnimState;
    [HideInInspector] public AnimState lastAnimState;

    [HideInInspector] public NavMeshAgent nma;

    // Start is called before the first frame update
    void Start()
    {
        anim = transform.Find("Model").GetComponent<Animator>();
        if (GetComponent<NavMeshAgent>())
        {
            nma = GetComponent<NavMeshAgent>();
        }
        damageDelegate = ApplyDamage;
        personalCanvas = transform.Find("Personal Canvas").gameObject;
        Debug.Log(gameObject.name + " set up");
    }

    // Update is called once per frame
    void Update()
    {
        Action();
    }

    public virtual void Action()
    {
        switch (currentActionState)
        {
            case ActionState.Attacking:
                if (attackCounter == 0)
                {
                    anim.Play("Attack 0");
                }
                attackCounter += Time.deltaTime;
                if (attackCounter >= (attackSpeed * 0.3f) && autoHitBox == null)
                {
                    autoHitBox = Instantiate(PrefabMaster.instance.playerAutoHitBox, transform.position + transform.forward + Vector3.up, transform.rotation);
                    autoHitBox.transform.parent = transform;
                    autoHitBox.GetComponent<CommonHitBox>().Connect();
                }
                if (attackCounter >= attackSpeed * 0.6f)
                {
                    CleanUpAutoHitBox();
                }
                if (attackCounter >= attackSpeed)
                {
                    attackCounter = 0;
                    Deactivate();
                    Request_ActionState(ActionState.Special_Transmission);
                }
                break;
            case ActionState.CheckQueue:
                if (queue.Count != 0)
                {
                    Debug.Log("Queue has something, executing");
                    queuedAction = queue[0];
                    queue.Remove(queuedAction);
                    queuedAction(null, Vector3.zero);
                }
                else
                {
                    Request_ActionState(ActionState.Idle);
                    Debug.Log("Queue Count is 0, returning to Idle");
                }
                break;
            case ActionState.Dead:
                deathCountout -= Time.deltaTime * GameManager.instance.ingameTimeScale;
                if (deathCountout <= 0)
                {
                    Debug.Log(gameObject.name + " removing itself due to death time out");
                    Destroy(gameObject);
                }
                break;
            case ActionState.Idle:
                CheckTarget();
                if (target_GO != null)
                {
                    Request_ActionState(ActionState.Special_Full);
                }
                break;
            case ActionState.MoveIntoRange:
                if (target_GO != null)
                {
                    if (Vector3.Distance(transform.position, target_GO.transform.position) > desiredRange)
                    {
                        nma.destination = target_GO.transform.position - (target_GO.transform.position - transform.position).normalized * desiredRange;
                        anim.Play("Walk_Forward_00");
                    }
                    else
                    {
                        Request_ActionState(lastActionState);
                    }
                }
                else
                {
                    Request_ActionState(ActionState.Idle);
                }
                break;
            case ActionState.MoveToDestination:
                nma.isStopped = false;
                anim.Play("Walk_Forward_00");
                if (Vector3.Distance(nma.destination, transform.position) < 0.1f)
                {
                    nma.isStopped = true;
                    anim.Play("Idle");
                    Debug.Log("Reached Destination, should initiated QueueCheck?");
                    Request_ActionState(ActionState.CheckQueue);
                }
                break;
            case ActionState.Special_Full:
                CheckTarget();
                if (currentCardState == CardState.Executing)
                {
                    executionLoop(this);
                }
                DeckMaster.instance.UseCard(this);
                break;
            case ActionState.Special_Transmission:
                multiPurposeCounter += Time.deltaTime;
                if (multiPurposeCounter >= transmissionTimeBetweenCards)
                {
                    Request_ActionState(ActionState.Idle);
                }
                break;
            case ActionState.Staggered:
                if (staggerTimer == 0)
                {
                    anim.Play("BeingHit");
                }
                staggerTimer += Time.deltaTime;
                if (staggerTimer >= 2)
                {
                    staggerTimer = 0;
                    Request_ActionState(ActionState.Idle);
                }
                break;
            case ActionState.Walking:
                break;
        }
    }

    public void ApplyDamage(int damage)
    {
        ApplyDamage_Health(damage);
        ApplyDamage_Stagger(damage);
    }

    public void ApplyDamage_Health(int damage)
    {
        if (currentHurtState == HurtState.Vurnerable)
        {
            currentHealth -= damage;
        }
    }

    public void ApplyDamage_Stagger(int damage)
    {
        staggerCount += damage;
        if (staggerCount >= staggerThreshold)
        {
            Request_ActionState(ActionState.Staggered);
            staggerCount = 0;
        }
        if (currentHealth <= 0)
        {
            Request_ActionState(ActionState.Dead);
            anim.enabled = false;
        }
        else
        {
            // check whether damage source is closer than original target, then update target if true
        }
    }

    public void ChangeAnimState(AnimState desiredAnimState)
    {
        lastAnimState = currentAnimState;
        currentAnimState = desiredAnimState;
    }

    public void CheckTarget()
    {
        if (target_GO == null)
        {
            target_GO = CombatManager.instance.RequestNewTarget(gameObject);
        }
        else if (target_GO.GetComponent<CombatIntelligence>().currentActionState == ActionState.Dead)
        {
            Debug.Log(gameObject.name + ": target ded, looking for new");
            target_GO = CombatManager.instance.RequestNewTarget(gameObject);
        }
    }

    public void CleanUpAutoHitBox()
    {
        if (autoHitBox != null)
        {
            Destroy(autoHitBox);
            autoHitBox = null;
        }
    }

    public void Deactivate()
    {
        currentCardState = CardState.Idle;
        executionLoop = null;
        Request_ActionState(ActionState.Special_Transmission);
    }

    // checks non player units on whether or not they are in range of something
    public bool InRange(float rangeToCheck)
    {
        if (gameObject.tag == "Player")
        {
            return true;
        }
        if (Vector3.Distance(transform.position, target_GO.transform.position) >= rangeToCheck)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public void QueueAction(CombatIntelligence target, Vector3 destination)
    {
        if(target != null)
        {
            queue.Add(QueueAction_Target);
            queue_targets.Add(target);
            queueCount++;
        }
        else if (destination != Vector3.zero)
        {
            Debug.Log("Received Queue Destination");
            queue.Add(QueueAction_Destination);
            queue_destinations.Add(destination);
            queueCount++;
        }
        else
        {
            Debug.LogError("QueueAction called on " + gameObject.name + " with both a target and a destination");
            Request_ActionState(ActionState.Special_Full);
        }
    }

    private void QueueAction_Destination(CombatIntelligence target, Vector3 destination)
    {
        Debug.Log("Executing Destionation Queue");
        nma.destination = queue_destinations[0];
        queue_destinations.Remove(queue_destinations[0]);
        Request_ActionState(ActionState.MoveToDestination);
    }

    private void QueueAction_Target(CombatIntelligence target, Vector3 destination)
    {
        if (queue_targets[0])
        {
            target_GO = queue_targets[0].gameObject;
            queue_targets.Remove(queue_targets[0]);
            Request_ActionState(ActionState.Special_Full);
        }
        else
        {
            Request_ActionState(ActionState.Idle);
        }
    }

    public virtual void Request_ActionState(ActionState desiredState)
    {
        CleanUpAutoHitBox();
        if (!executionTempObject_isPersistent)
        {
            Destroy(executionTempObject);
        }
        switch (desiredState)
        {
            case ActionState.Attacking:
                attackCounter = 0;
                CleanUpAutoHitBox();
                transform.LookAt(target_GO.transform.position);
                break;
            case ActionState.CheckQueue:
                break;
            case ActionState.Dead:
                deathCountout = 10;
                break;
            case ActionState.Idle:
                anim.Play("Idle");
                break;
            case ActionState.MoveIntoRange:
                Deactivate();
                break;
            case ActionState.MoveToDestination:
                break;
            case ActionState.Special_Full:
                break;
            case ActionState.Special_Transmission:
                multiPurposeCounter = 0;
                break;
            case ActionState.Staggered:
                multiPurposeCounter = 0;
                nma.speed = nma_defaultSpeed;
                nma.angularSpeed = nma_defaultAngularSpeed;
                break;
            case ActionState.Walking:
                break;
        }
        Set_ActionState(desiredState);
    }

    public virtual void Set_ActionState(ActionState desiredState)
    {
        lastActionState = currentActionState;
        currentActionState = desiredState;
    }
}
