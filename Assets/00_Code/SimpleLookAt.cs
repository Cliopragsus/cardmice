﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleLookAt : MonoBehaviour
{
    Vector3 reversedCameraDirection;

    void Update()
    {
        reversedCameraDirection = (Camera.main.transform.position - transform.position) * -1;
        gameObject.transform.LookAt(Camera.main.transform.position);
    }
}
