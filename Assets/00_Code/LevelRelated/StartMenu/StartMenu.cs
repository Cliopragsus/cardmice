﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEngine.SceneManagement;

public class StartMenu : MonoBehaviour
{
    public GameObject mainWindow;
    public GameObject saveWindow;
    public GameObject loadWindow;

    public GameObject[] saveWindows = new GameObject[3];
    public GameObject[] loadWindows = new GameObject[3];

    public enum MenuStates { Main, Save, Load };
    public MenuStates menuState;


    public void ChangeMenuState(MenuStates desiredState)
    {
        switch (desiredState)
        {
            case MenuStates.Main:
                mainWindow.SetActive(true);
                saveWindow.SetActive(false);
                loadWindow.SetActive(false);
                break;
            case MenuStates.Save:
                if (SceneManager.GetActiveScene().ToString() != "Start")
                {
                    mainWindow.SetActive(false);
                    loadWindow.SetActive(false);
                    saveWindow.SetActive(true);
                    for (int i = 0; i < 3; i++)
                    {
                        if (File.Exists(Application.dataPath + "/SaveStates/save" + i + ".txt"))
                        {
                            Loader.SaveState save = Loader.saveStates[i];
                            float minutes = Mathf.FloorToInt(save.playTime / 60);
                            float hours = Mathf.FloorToInt(minutes / 60);
                            saveWindows[i].GetComponentInChildren<Text>().text =
                                save.lastScene.ToString()
                                + "\n"
                                + hours + ":" + minutes + ":" + Mathf.RoundToInt(save.playTime - (hours * 360) - (minutes * 60));
                        }
                        else
                        {
                            saveWindows[i].GetComponentInChildren<Text>().text = "EMPTY";
                        }
                    }
                }
                break;
            case MenuStates.Load:
                mainWindow.SetActive(false);
                saveWindow.SetActive(false);
                loadWindow.SetActive(true);
                for (int i = 0; i < 3; i++)
                {
                    if(File.Exists(Application.dataPath + "/SaveStates/save" + i + ".txt"))
                    {
                        if (File.Exists(Application.dataPath + "/SaveStates/save" + i + ".txt"))
                        {
                            Loader.SaveState save = Loader.saveStates[i];
                            float minutes = Mathf.FloorToInt(save.playTime / 60);
                            float hours = Mathf.FloorToInt(minutes / 60);
                            loadWindows[i].GetComponentInChildren<Text>().text =
                                save.lastScene.ToString()
                                + "\n"
                                + hours + ":" + minutes + ":" + Mathf.RoundToInt(save.playTime - (hours * 360) - (minutes * 60));
                        }
                        else
                        {
                            loadWindows[i].GetComponentInChildren<Text>().text = "EMPTY";
                        }
                    }
                    else
                    {
                        loadWindows[i].SetActive(false);
                    }
                }
                break;
            default:
                break;
        }
    }

    public void ToggleMenu()
    {
        GameManager.instance.PauseMenu_Toggle();
    }

    /// <summary>
    /// ///////////////////////////// MENU BUTTONS /////////////////////////////////////////
    /// </summary>

    public void ExitGame()
    {
        Application.Quit();
    }

    public void ReturnToMain()
    {
        ChangeMenuState(MenuStates.Main);
    }

    public void ResetGame()
    {
        Loader.Load(Loader.currentScene);
    }

    public void SaveState_Save(int index)
    {
        Loader.SaveState_Save(index);
        float minutes = Mathf.FloorToInt(Loader.currentSaveState.playTime / 60);
        float hours = Mathf.FloorToInt(minutes / 60);
        saveWindows[index].GetComponentInChildren<Text>().text = Loader.currentSaveState.lastScene.ToString()
            + "\n"
            + hours + ":" + minutes + ":" + Mathf.RoundToInt(Loader.currentSaveState.playTime - (hours * 360) - (minutes * 60));
    }

    public void SaveState_Load(int index)
    {
        Loader.SaveState_Load(index);
    }

    public void StartNewGame() // prepares new savefile for the first load
        // to do: create a textfile that will be taken in instead for easier manipulation
    {
        Loader.SaveState s = new Loader.SaveState();
        Loader.currentPlayTime = 0;

        s.socketedCards = new List<List<DeckMaster.Card>>();
        s.socketedCards.Add(new List<DeckMaster.Card>());
        s.socketedCards.Add(new List<DeckMaster.Card>());
        s.socketedCards.Add(new List<DeckMaster.Card>());

        s.frame_player = DeckMaster.Frame.Player_00_Basic;
        s.socketedCards[0].Add(DeckMaster.Card.Dash);
        s.socketedCards[0].Add(DeckMaster.Card.RageFunnel);
        s.socketedCards[0].Add(DeckMaster.Card.Dash);
        s.allyOne = true;
        s.frame_ally0 = DeckMaster.Frame.Ally_00_Basic;
        s.socketedCards[1].Add(DeckMaster.Card.Dash);
        s.socketedCards[1].Add(DeckMaster.Card.Dash);
        s.socketedCards[1].Add(DeckMaster.Card.BasicAttack);

        Loader.currentSaveState = s;
        string json = JsonUtility.ToJson(s);
        Debug.Log(json);
        Loader.Load(Loader.Scene.Hub);
    }

    public void ViewSaveStates()
    {
        ChangeMenuState(MenuStates.Save);
    }

    public void ViewLoadStates()
    {
        ChangeMenuState(MenuStates.Load);
    }
}
