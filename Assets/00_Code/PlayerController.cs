﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : CombatIntelligence
{
    public float sensitivity;

    public GameObject playerCam;
    public GameObject eyes;
    private float mX;
    private float mY;

    private GameObject model;

    // Start is called before the first frame update
    void Start()
    {
        model = transform.Find("Model").gameObject;
        anim = model.GetComponent<Animator>();
        damageDelegate = ApplyDamage;
    }

    // Update is called once per frame
    void Update()
    {
        if (currentCardState == CardState.Executing)
        {
            executionLoop(this);
        }
        if (!DeckMaster.instance.board.activeSelf && GameManager.instance.currentGameState == GameManager.GameState.InGame)
        {
            //Attack();
            //CameraTurning();
            GetTarget();
            Special();
        }

        Action();
    }

    public override void Action()
    {
        switch (currentActionState)
        {
            case ActionState.Walking:
                //Movement();
                MovementRelativeToCamera();
                MoveAnimControl();
                break;
            case ActionState.Attacking:
                MovementRelativeToCamera();

                //Movement();
                attackCounter += Time.deltaTime;
                if (attackCounter >= (attackSpeed * 0.3f) && autoHitBox == null)
                {
                    autoHitBox = Instantiate(PrefabMaster.instance.playerAutoHitBox, model.transform.position + model.transform.forward + Vector3.up, model.transform.rotation);
                    autoHitBox.transform.parent = transform;
                    autoHitBox.GetComponent<CommonHitBox>().Connect();
                    autoHitBox.transform.parent = model.transform;
                }
                if (attackCounter >= attackSpeed * 0.6f)
                {
                    Destroy(autoHitBox);
                    autoHitBox = null;
                }
                if (attackCounter >= attackSpeed)
                {
                    if (Input.GetMouseButton(0))
                    {
                        attackCounter = 0;
                    }
                    else
                    {
                        attackCounter = 0;
                        Request_ActionState(ActionState.Walking);
                    }
                }
                break;
            case ActionState.Special_Full:
                break;
            case ActionState.Special_Transmission:
                multiPurposeCounter += Time.deltaTime * GameManager.instance.ingameTimeScale;
                if (multiPurposeCounter >= 0.3f)
                {
                    Request_ActionState(ActionState.Walking);
                }
                MovementRelativeToCamera();

                //Movement();
                break;
            case ActionState.Dead:
                break;
            default:
                break;
        }
    }

    public void Attack()
    {
        if (currentActionState == ActionState.Walking)
        {
            if (Input.GetMouseButton(0))
            {
                if (currentActionState != ActionState.Attacking)
                {
                    Request_ActionState(ActionState.Attacking);
                }
            }
        }
    }

    public void CameraTurning()
    {
        if (!DeckMaster.instance.board.activeSelf)
        {
            mX = Input.GetAxis("Mouse X") * sensitivity;
            mY -= Input.GetAxis("Mouse Y") * sensitivity;
            mY = Mathf.Clamp(mY, -30, 75);

            transform.Rotate(0, mX, 0);
            eyes.transform.localRotation = Quaternion.Euler(mY, 0, 0);
        }
    }

    public void GetTarget()
    {
        int layerMask = 1 << 9;
        RaycastHit hitInfo;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hitInfo, layerMask))
        {
            if (hitInfo.collider.gameObject.GetComponent<CombatIntelligence>())
            {
                target_GO = hitInfo.collider.gameObject;
            }
        }

        int layerMask2 = 1 << 10;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hitInfo, layerMask2))
        {
            target_Vec3 = hitInfo.point;
        }
    }

    public void Movement()
    {
        transform.Translate(Input.GetAxis("Horizontal") * Time.deltaTime * moveSpeed, 0, Input.GetAxis("Vertical") * Time.deltaTime * moveSpeed);
    }

    public void MovementRelativeToCamera()
    {
        Vector3 viewPos = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        model.transform.LookAt(transform.position + viewPos);

        float horizontalAxis = Input.GetAxis("Horizontal");
        float verticalAxis = Input.GetAxis("Vertical");

        var camera = Camera.main;

        var forward = camera.transform.forward;
        var right = camera.transform.right;

        forward.y = 0f;
        right.y = 0f;
        forward.Normalize();
        right.Normalize();

        var desiredMoveDirection = forward * verticalAxis + right * horizontalAxis;

        transform.Translate(desiredMoveDirection * moveSpeed * Time.deltaTime * GameManager.instance.ingameTimeScale);
    }

    public void MoveAnimControl()
    {
        if (Input.GetKey(KeyCode.W))
        {
            if (currentAnimState != AnimState.Forward && currentActionState != ActionState.Attacking)
            {
                anim.Play("Walk_Forward_00");
                ChangeAnimState(AnimState.Forward);
            }
        }

        if (Input.GetKey(KeyCode.A))
        {
            if (currentAnimState != AnimState.Left && currentAnimState != AnimState.Forward && currentAnimState != AnimState.Backwards && currentActionState != ActionState.Attacking)
            {
                anim.Play("Walk_Forward_00");
                ChangeAnimState(AnimState.Forward);
            }
        }

        if (Input.GetKey(KeyCode.S))
        {
            if (currentAnimState != AnimState.Backwards && currentActionState != ActionState.Attacking)
            {
                anim.Play("Walk_Forward_00");
                ChangeAnimState(AnimState.Forward);
            }
        }

        if (Input.GetKey(KeyCode.D))
        {
            if (currentAnimState != AnimState.Right && currentAnimState != AnimState.Forward && currentAnimState != AnimState.Backwards && currentActionState != ActionState.Attacking)
            {
                anim.Play("Walk_Forward_00");
                ChangeAnimState(AnimState.Forward);
            }
        }

        if (Input.GetKeyUp(KeyCode.W) && currentActionState != ActionState.Attacking)
        {
            anim.Play("Idle");
            ChangeAnimState(AnimState.Idle);
        }
        if (Input.GetKeyUp(KeyCode.A) && currentActionState != ActionState.Attacking)
        {
            anim.Play("Idle");
            ChangeAnimState(AnimState.Idle);
        }
        if (Input.GetKeyUp(KeyCode.S) && currentActionState != ActionState.Attacking)
        {
            anim.Play("Idle");
            ChangeAnimState(AnimState.Idle);
        }
        if (Input.GetKeyUp(KeyCode.D) && currentActionState != ActionState.Attacking)
        {
            anim.Play("Idle");
            ChangeAnimState(AnimState.Idle);
        }
    }

    public override void Request_ActionState(ActionState desiredState)
    {
        CleanUpAutoHitBox();
        switch (desiredState)
        {
            case ActionState.Attacking:
                attackCounter = 0;
                CleanUpAutoHitBox();
                if (currentAnimState != AnimState.Attack)
                {
                    anim.Play("Attack 0");
                    ChangeAnimState(AnimState.Attack);
                }
                Set_ActionState(desiredState);
                break;
            case ActionState.Idle:
                Set_ActionState(desiredState);
                anim.Play("Idle");
                ChangeAnimState(AnimState.Idle);
                break;
            case ActionState.MoveIntoRange:
                Set_ActionState(desiredState);
                break;
            case ActionState.Special_Full:
                Set_ActionState(desiredState);
                break;
            case ActionState.Special_Transmission:
                multiPurposeCounter = 0;
                currentAnimState = AnimState.Cast;
                Set_ActionState(desiredState);
                break;
            case ActionState.Staggered:
                Set_ActionState(desiredState);
                break;
            case ActionState.Walking:
                Set_ActionState(desiredState);
                break;
            case ActionState.Dead:
                Set_ActionState(desiredState);
                CleanUpAutoHitBox();
                break;
            default:
                break;
        }
        Debug.Log("Changed ActionState from " + lastActionState + " to " + currentActionState);
    }

    public override void Set_ActionState(ActionState desiredState)
    {
        lastActionState = currentActionState;
        currentActionState = desiredState;
    }

    public void Special()
    {
        if (Input.GetMouseButtonDown(1) || Input.GetKeyDown(KeyCode.B))
        {
            DeckMaster.instance.UseCard(this);
        }
    }
}

