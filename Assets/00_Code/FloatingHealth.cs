﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloatingHealth : MonoBehaviour
{
    public Sprite[] hearthStates;
    public CombatIntelligence parentCI;
    public float p1;
    public float p2;
    public GameObject currentHearth;
    public Vector3 originPosition;
    public float distanceFromModel = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        parentCI = transform.parent.GetComponent<CombatIntelligence>();
        p1 = 100 / parentCI.maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        p2 = (parentCI.currentHealth * p1) / 5;
        if (p2 < 0)
        {
            p2 = 0;
        }
        for (int i = 0; i < 20; i++)
        {
            if (p2 >= i)
            {
                currentHearth.GetComponent<SpriteRenderer>().sprite = hearthStates[i];
            }
        }

        transform.position = parentCI.gameObject.transform.position + originPosition + (Camera.main.transform.position - parentCI.gameObject.transform.position).normalized * distanceFromModel;
        //transform.LookAt(-Camera.main.transform.position);
    }
}
