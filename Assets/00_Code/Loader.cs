﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System;

public static class Loader
{
    public enum Scene
    {
        Start,
        LoadScreen,
        Hub,
        CombatArena,
        Test_Initialization
    }

    public static Scene currentScene;
    public static Scene sceneToBeLoaded;

    public static string json;
    public static SaveState currentSaveState;
    public static SaveState[] saveStates = new SaveState[3];

    public static float currentPlayTime;

    [Serializable]
    public class SaveState
    {
        // orga
        public Scene lastScene;
        public float playTime;

        // cards
        public int fireBall;
        public int dash;
        public int rageFunnel;

        // characters
        public bool allyOne;
        public bool allyTwo;

        public DeckMaster.Frame frame_player;
        public DeckMaster.Frame frame_ally0;
        public DeckMaster.Frame frame_ally1;

        public List<List<DeckMaster.Card>> socketedCards = new List<List<DeckMaster.Card>>();
        public string socketedCardsString;
    }

    public static void Init()
    {
        for (int i = 0; i < 3; i++)
        {
            if (File.Exists(Application.dataPath + "/SaveStates/save" + i + ".txt"))
            {
                saveStates[i] = JsonUtility.FromJson<SaveState>(File.ReadAllText(Application.dataPath + "/SaveStates/save" + i + ".txt"));
            }
        }
    }

    public static void SaveState_Save(int desiredSave)
    {
        currentSaveState.playTime = currentPlayTime;
        currentSaveState.lastScene = currentScene;
        GameManager.instance.ConvertListOfListToString(currentSaveState.socketedCards);
        json = JsonUtility.ToJson(currentSaveState);
        File.WriteAllText(Application.dataPath + "/SaveStates/save" + desiredSave + ".txt", json);
        saveStates[desiredSave] = currentSaveState;
        Debug.Log("Saved GameState: " + desiredSave + " " + json);
    }

    public static void SaveState_Load(int desiredSave)
    {
        if (saveStates[desiredSave] != null)
        {
            currentSaveState = saveStates[desiredSave];
            currentPlayTime = currentSaveState.playTime;
            GameManager.instance.ConvertStringToListOfList(currentSaveState.socketedCardsString);
            Load(currentSaveState.lastScene);
        }
        else
        {
            Debug.LogWarning("No corresponding Savefile found!");
        }
    }

    private delegate void onLoaderCallback(Scene desiredScene);

    public static void Load(Scene desiredScene)
    {
        if (currentScene.ToString() != "Start")
        {

            GameManager.instance.UpdateCurrentSave();
        }
        sceneToBeLoaded = desiredScene;
        SceneManager.LoadScene(Scene.LoadScreen.ToString());
    }

    public static void LoaderCallback()
    {
        Debug.Log("LoaderCallback : sceneToBeLoaded = " + sceneToBeLoaded.ToString());
        currentScene = sceneToBeLoaded;
        SceneManager.LoadScene(sceneToBeLoaded.ToString());
    }
}
