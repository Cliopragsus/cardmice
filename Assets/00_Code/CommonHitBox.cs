﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommonHitBox : MonoBehaviour
{
    public CombatManager.Affilitation affiliation;
    public CombatIntelligence parentCI;
    public string hitBoxName;
    public int damage;

    public void Connect()
    {
        parentCI = transform.parent.gameObject.GetComponent<CombatIntelligence>();
        affiliation = parentCI.affiliation;
        damage = parentCI.attackDamage;
    }

    private void OnTriggerEnter(Collider other)
    {
        switch (affiliation)
        {
            case CombatManager.Affilitation.None:
                break;
            case CombatManager.Affilitation.Ally:
                if (other.gameObject.GetComponent<CombatIntelligence>())
                {
                    CombatIntelligence enemy = other.gameObject.GetComponent<CombatIntelligence>();
                    if (enemy.affiliation == CombatManager.Affilitation.Enemy)
                    {
                        enemy.damageDelegate(damage);
                        //Debug.Log("Hit an enemy with AUTO");
                    }
                }
                break;
            case CombatManager.Affilitation.Enemy:
                if (other.gameObject.GetComponent<CombatIntelligence>())
                {
                    if (other.gameObject.GetComponent<CombatIntelligence>())
                    {
                        CombatIntelligence ally = other.gameObject.GetComponent<CombatIntelligence>();
                        if (ally.affiliation == CombatManager.Affilitation.Ally)
                        {
                            ally.damageDelegate(damage);
                            //Debug.Log("Hit an ally with AUTO");
                        }
                    }
                }
                break;
            default:
                break;
        }
    }
}
